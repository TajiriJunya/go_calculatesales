package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func readFile(branchName map[string]string, branchCales map[string]int64, listPath string) {

	fp, err := os.Open(listPath)

	if err != nil {
		panic("実行できません：支店定義ファイルの展開に失敗しました。パスを確認してください")
	}

	defer fp.Close()

	scanner := bufio.NewScanner(fp)
	fileformatCode, _ := regexp.Compile("^[0-9]{3}$")
	for scanner.Scan() {
		entry := scanner.Text()
		slice := strings.Split(entry, ",")
		if !fileformatCode.MatchString(slice[0]) || len(slice) != 2 {
			panic("支店定義ファイルのフォーマットが不正です")
		}
		code := slice[0]
		name := slice[1]
		branchName[code] = name
		branchCales[code] = 0
	}
}

func calsal(calespath string, branchCales map[string]int64) {

	fis, err := ioutil.ReadDir(calespath)

	if err != nil {
		panic("ディレクトリの展開に失敗しました。パスを確認してください。")
	}

	re, _ := regexp.Compile("^[0-9]{8}.rcd$")
	var rcdlist []string
	for _, fi := range fis {
		if re.MatchString(fi.Name()) {
			rcdlist = append(rcdlist, fi.Name())
		}
	}

	if len(rcdlist) == 0 {
		panic("売り上げファイルが発見できませんでした。\nファイル名のフォーマットは8桁数字.rcdです")
	}

	for nc := 0; nc < len(rcdlist)-1; nc++ {
		a := rcdlist[nc]
		a1 := rcdlist[nc+1]
		stra := a[0:8]
		stra1 := a1[0:8]
		inta, _ := strconv.Atoi(stra)
		inta1, _ := strconv.Atoi(stra1)

		if (inta1 - inta) != 1 {
			panic("売り上げファイル名が連番になっていません")
		}
	}

	for _, fi := range rcdlist {

		fllpath := filepath.Join(calespath, fi)
		fp, err := os.Open(fllpath)
		if err != nil {
			panic(err)
		}
		scanner := bufio.NewScanner(fp)
		var key string
		var val int
		fileformatCode, _ := regexp.Compile("^[0-9]{3}$")
		fileformatsales, _ := regexp.Compile("^[0-9]{1,}$")
		for a := 0; scanner.Scan(); a++ {
			if a == 0 {
				key = scanner.Text()
				if !fileformatCode.MatchString(key) {
					fp.Close()
					panic("売り上げファイルのフォーマットが不正です")
				}
			} else if a == 1 {
				if !fileformatsales.MatchString(scanner.Text()) {
					fp.Close()
					panic("売り上げファイルのフォーマットが不正です")
				}
				val, _ = strconv.Atoi(scanner.Text())
			} else {
				fp.Close()
				panic("売り上げファイルのフォーマットが不正です")
			}
		}
		bef := branchCales[key]
		aft := bef + int64(val)
		if aft > 10000000000 {
			panic("合計金額が10桁を超えています")
		}
		branchCales[key] = aft

		fp.Close()
	}
}

func writeFile(branchName map[string]string, branchCales map[string]int64, outPath string) {

	file, err := os.Create(outPath)
	if err != nil {
		log.Fatal(err) //ファイルが開けなかったときエラー出力
	}
	defer file.Close()

	for _, key := range sortMap(branchName) {
		c := int(branchCales[key])
		s := key + "," + branchName[key] + "," + strconv.Itoa(c) + "\n"
		file.WriteString(s)
		fmt.Println(s)
	}
}

func sortMap(branchName map[string]string) []string {

	slice := make([]string, len(branchName))
	index := 0
	for key := range branchName {
		slice[index] = key
		index++
	}
	sort.Strings(slice)
	return slice
}

func main() {
	listPath := "../new/branch.lst"
	calespath := "C:/Users/tajiri.junya/Desktop/go言語学習/new"
	outPath := "C:/Users/tajiri.junya/Desktop/go言語学習/new/branchGo.out"
	branchName := map[string]string{}
	branchCales := map[string]int64{}

	readFile(branchName, branchCales, listPath)
	calsal(calespath, branchCales)
	writeFile(branchName, branchCales, outPath)

}
